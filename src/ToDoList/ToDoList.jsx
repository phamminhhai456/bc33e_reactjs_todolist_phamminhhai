import React, { Component } from "react";
import { connect } from "react-redux";
import { ThemeProvider } from "styled-components";
import { Heading3 } from "./Components/Heading";
import { arrTheme } from "./Themes/ThemeManager";
import { TextField } from "./Components/TextField";
import { Button } from "./Components/Button";
import { Table, Tr, Th, Thead, Tbody } from "./Components/Table";
import { Dropdown } from "./Components/Dropdown";
import { Container } from "./Components/Container";
import {
  addTaskAction,
  changeThemeAction,
  deleteTaskAction,
  doneTaskAction,
  editTaskAction,
  rejectTaskAction,
  updateTaskAction,
} from "../redux/actions/ToDoListAction";

class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };

  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                className="ml-1"
                onClick={() => {
                  this.setState(
                    {
                      disable: false,
                    },
                    () => {
                      this.props.dispatch(editTaskAction(task));
                    }
                  );
                }}
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(rejectTaskAction(task.id));
                }}
              >
                <i className="fa fa-undo"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return (
        <option key={index} value={theme.id}>
          {theme.name}
        </option>
      );
    });
  };
  render() {
    return (
      <div>
        <ThemeProvider theme={this.props.themeToDoList}>
          <Container className="w-50">
            <Dropdown
              onChange={(e) => {
                let { value } = e.target;
                this.props.dispatch(changeThemeAction(value));
              }}
            >
              {this.renderTheme()}
            </Dropdown>
            <Heading3>To Do List</Heading3>
            <TextField
              value={this.state.taskName}
              onChange={(e) => {
                this.setState({
                  taskName: e.target.value,
                });
              }}
              name="taskName"
              label="task name"
              className="w-50"
            />
            <Button
              className="ml-2"
              onClick={() => {
                let { taskName } = this.state;

                let newTask = {
                  id: Date.now(),
                  taskName: taskName,
                  done: false,
                };
                this.props.dispatch(addTaskAction(newTask));
              }}
            >
              <i className="fa fa-plus"></i> Add task
            </Button>
            {this.state.disabled ? (
              <Button
                className="ml-2"
                onClick={() =>
                  this.props.dispatch(updateTaskAction(this.state.taskName))
                }
              >
                <i className="fa fa-upload"></i> Update task
              </Button>
            ) : (
              <Button
                className="ml-2"
                onClick={() => {
                  let { taskName } = this.state;
                  this.setState(
                    {
                      disable: true,
                      taskName: "",
                    },
                    () => {
                      this.props.dispatch(updateTaskAction(taskName));
                    }
                  );
                }}
              >
                <i className="fa fa-upload"></i> Update task
              </Button>
            )}
            <hr />
            <Heading3>Task To Do</Heading3>
            <Table>
              <Thead>{this.renderTaskToDo()}</Thead>
            </Table>
            <Heading3>Task Complete</Heading3>
            <Table>
              <Thead>{this.renderTaskCompleted()}</Thead>
            </Table>
          </Container>
        </ThemeProvider>
      </div>
    );
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
